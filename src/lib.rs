use std::ops::Add;
use std::ops::Mul;
use std::ops::Sub;
use num_traits::Float;
pub mod color;
pub mod coords;
use coords::Point3;
use coords::Vec3;

type Range<T> = (T,T);

#[derive(Debug,Clone,Copy)]
pub struct Ray<T> {
  pub orig: Point3<T>,
  pub dir:  Vec3<T>,
}

impl<T: Mul<Output = T> + Copy + Add<Output = T>> Ray<T> {
  fn at(&self, t: T ) -> Point3<T> {
    self.orig + (self.dir * t)
  }
}

#[derive(Debug,Clone,Copy)]
pub struct HitRec<T> {
  pub p: Point3<T>,
  pub normal: Vec3<T>,
  pub t: T,
  pub front_face: bool,
}

impl<T: Float> HitRec<T> {
  fn set_face_normal( &mut self, ray: &Ray<T>, outward_normal: &Vec3<T> ) {
    self.front_face = Vec3::dot(&ray.dir, outward_normal).is_sign_negative();
    if self.front_face {
      self.normal = *outward_normal;
    } else {
      self.normal = -*outward_normal;
    }
  }
}

pub trait Hittable<T> {
  fn hit( &self, ray: &Ray<T>, t_min_max: Range<T>) -> Option<HitRec<T>>;
}

pub struct HittableWorld<T> {
  pub world: Vec<Box<dyn Hittable<T>>>
}

impl<T: Copy> HittableWorld<T> {
  pub fn hit_all (&self, ray: &Ray<T>, (t_min,t_max): Range<T>) -> Option<HitRec<T>> {
    self.world.iter().fold((None, t_max),
      | (last_hit, closest_so_far), elem| {
        match elem.hit(ray, (t_min, closest_so_far)) {
          None => (last_hit, closest_so_far),
          Some( hit_res ) => (Some(hit_res), hit_res.t),
        }
      } ).0
  }
}

pub struct Sphere<T> {
  pub center: Point3<T>,
  pub radius: T,
}

impl<T: Sub<Output = T> + Float> Hittable<T> for Sphere<T> {
  fn hit( &self, ray: &Ray<T>, (t_min,t_max): Range<T>) -> Option<HitRec<T>> {
    let oc = ray.orig - self.center;
    let a = ray.dir.length_sq();
    let half_b = Vec3::dot( &oc, &ray.dir );
    let c = oc.length_sq() - self.radius.powi(2);

    let disc = half_b.powi(2) - a*c;

    if disc.is_sign_negative() {
      return None;
    } else {
      let sqrtd = disc.sqrt();
      let (root_0, root_1) = ((-half_b - sqrtd) / a, (-half_b + sqrtd) / a);
      let r_at_0 = ray.at(root_0);
      let mut hit_rec_0 = HitRec{
        t: root_0,
        p: r_at_0,
        normal: (r_at_0 - self.center) / self.radius,
        front_face: false,
      };
      let outward_normal_0 = (r_at_0 - self.center) / self.radius;

      if ( root_0 < t_min ) || ( t_max < root_0 ) {
        let r_at_1 = ray.at(root_1);
        let mut hit_rec_1 = HitRec{
          t: root_1,
          p: r_at_1,
          normal: (r_at_1 - self.center) / self.radius,
          front_face: false
        };

        let outward_normal_1 = (r_at_1 - self.center) / self.radius;

        if ( root_1 < t_min ) || ( t_max < root_1 ) {
          return None;
        } else {
          hit_rec_1.set_face_normal(ray, &outward_normal_1);
          return Some( hit_rec_1 );
        }
      } else {
        hit_rec_0.set_face_normal(ray, &outward_normal_0);
        return Some( hit_rec_0 );
      }
    }
  }
}

