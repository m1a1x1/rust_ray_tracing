use std::ops::Add;
use std::ops::Mul;
use num_traits::identities::Zero;

#[derive(Debug,Clone,Copy,PartialEq)]
pub struct ColorRGB<T>{
  pub r: T,
  pub g: T,
  pub b: T,
}

impl<T: Mul + Mul<Output = T> + Copy> Mul<T> for ColorRGB<T> {
  type Output = ColorRGB<T>;

  fn mul(self, other: T) -> ColorRGB<T> {
    ColorRGB {
      r: self.r * other,
      g: self.g * other,
      b: self.b * other,
    }
  }
}

impl<T: Zero> Zero for ColorRGB<T> {
  fn zero() -> ColorRGB<T> {
    ColorRGB {
      r: T::zero(),
      g: T::zero(),
      b: T::zero(),
    }
  }
  fn is_zero(&self) -> bool {
    self.r.is_zero() && self.g.is_zero() && self.b.is_zero()
  }
}

impl<T: Add + Add<Output = T>> Add<ColorRGB<T>> for ColorRGB<T> {
  type Output = ColorRGB<T>;

  fn add(self, other: ColorRGB<T>) -> ColorRGB<T> {
    ColorRGB {
      r: self.r + other.r,
      g: self.g + other.g,
      b: self.b + other.b,
    }
  }
}

impl Into<ColorRGB<u8>> for &ColorRGB<f64> {
  fn into(self) -> ColorRGB<u8> {
    ColorRGB {
      r: (255.999 * self.r).floor() as u8,
      g: (255.999 * self.g).floor() as u8,
      b: (255.999 * self.b).floor() as u8,
    }
  }
}

