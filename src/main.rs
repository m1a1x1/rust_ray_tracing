use std::fs::File;
use std::io::Write;
use ndarray::{Array2};
use clap::{Arg, App};

mod color;
use color::ColorRGB;
//mod coords;
use rust_ray_tracing::coords::Point3;
use rust_ray_tracing::coords::Vec3;
use rust_ray_tracing::Sphere;
use rust_ray_tracing::Ray;
use rust_ray_tracing::HittableWorld;

const DEFAULT_WIDTH  : usize = 256;
const DEFAULT_HEIGHT : usize = 256;
const DEFAULT_OUTFNAME : &str = "render.ppm";

#[derive(Debug,Clone,Copy)]
struct Camera {
  height: f64,
  width: f64,
  focal_length: f64,
}

fn vec_to_color( vec: Vec3<f64> ) -> ColorRGB<f64> {
  ColorRGB {
    r: vec.x,
    g: vec.y,
    b: vec.z,
  }
}

fn ray_color (ray: &Ray<f64>, world: &HittableWorld<f64>) -> ColorRGB<f64> {
  match world.hit_all( ray, (0.0, f64::MAX) ) {
    Some( hit_rec ) => ( vec_to_color(hit_rec.normal) + ColorRGB{ r: 1.0, g: 1.0, b: 1.0 } ) * 0.5,
    None => {
        let unit_dir = ray.dir.unit_vector();
        let t = 0.5 * (unit_dir.y + 1.0);
        ColorRGB {r:1.0,g:1.0,b:1.0} * (1.0-t) + ColorRGB {r:0.5,g:0.7,b:1.0} * t
      }
  }
}

fn screen_to_file ( screen: &Array2<ColorRGB<f64>>, fname: &str )
{
  let mut outfile = File::create(fname)
                      .expect("Unable to create a file");
  let s_shape = screen.shape();
  let s_h = s_shape[0];
  let s_w = s_shape[1];

  outfile.write_all( format!("P3\n{width} {height}\n255\n", width=s_w, height=s_h ).as_bytes() )
                      .expect("Unable to write to file");

  for row in screen.outer_iter().rev() {
    for pix in row.iter() {
      let pix_u8: ColorRGB<u8> = pix.into();
      write!(outfile, "{} {} {}\n", pix_u8.r,
                                    pix_u8.g,
                                    pix_u8.b)
        .expect("Unable to write to file");
    }
  }
}

fn main() {
  let matches = App::new("Rust Ray Tracing")
          .version("0.1.0")
          .author("Maksim Tolkachev <m.tolkachev@metrotek.ru>")
          .about("Testing Rey Tracing in rust")
          .arg(Arg::new("file")
                   .short('F')
                   .long("file")
                   .takes_value(true)
                   .about("output .ppm file")
                   .default_value(&DEFAULT_OUTFNAME))
          .arg(Arg::new("width")
                   .short('W')
                   .long("width")
                   .takes_value(true)
                   .about("Width of output render")
                   .default_value(&DEFAULT_WIDTH.to_string()))
          .arg(Arg::new("height")
                   .short('H')
                   .long("height")
                   .takes_value(true)
                   .about("Height of output render")
                   .default_value(&DEFAULT_HEIGHT.to_string()))
          .get_matches();

    let fname: &str = matches.value_of("file").unwrap();
    let width: usize = matches.value_of_t("width")
                              .unwrap_or_else( |e| e.exit() );
    let height: usize = matches.value_of_t("height")
                               .unwrap_or_else( |e| e.exit() );

    let ratio: f64 = width as f64 / height as f64;

    let mut screen = Array2::<ColorRGB<f64>>::zeros((height, width));
    let     camera = Camera {
                       height: 2.0,
                       width: ratio * 2.0,
                       focal_length: 1.0
                     };

    let origin = Point3 { x: 0.0, y: 0.0, z: 0.0};
    let horizontal = Vec3 { x: camera.width,  y: 0.0, z: 0.0};
    let vertical   = Vec3 { x: 0.0, y: camera.height, z: 0.0};
    let lower_left_corner = origin -
                            horizontal / 2.0 -
                            vertical / 2.0 -
                            Vec3{ x: 0.0, y: 0.0, z: camera.focal_length };

    let world = HittableWorld{ world: vec![
      Box::new(Sphere { center: Point3{ x:0.0, y:0.0, z: -1.0}, radius: 0.5 }),
      Box::new(Sphere { center: Point3{ x:0.0, y:-100.5, z: -1.0}, radius: 100.0 }),
      ]
    };

    for j in 0..height {
      for i in 0..width {
        let u = i as f64 / (width-1) as f64;
        let v = j as f64 / (height-1) as f64;
        let ray = Ray { orig: origin,
                        dir: lower_left_corner +
                        horizontal*u +
                        vertical*v -
                        origin,
                      };
        let pix = ray_color(&ray, &world);
        screen[[j,i]] = pix;
      }
    }

    screen_to_file( &screen, fname );

    println!("Done");
}
