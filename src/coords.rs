use std::ops::Add;
use std::ops::Mul;
use std::ops::Div;
use std::ops::Sub;
use num_traits::Float;
use std::ops::Neg;

#[derive(Debug,Clone,Copy,PartialEq)]
pub struct Coords<T> {
  pub x: T,
  pub y: T,
  pub z: T,
}

impl<T: Add<Output = T>> Add<Coords<T>> for Coords<T> {
  type Output = Self;

  fn add(self, other: Self) -> Self {
    Coords {
      x: self.x + other.x,
      y: self.y + other.y,
      z: self.z + other.z,
    }
  }
}

impl<T: Mul<Output = T> + Copy> Mul<T> for Coords<T> {
  type Output = Self;

  fn mul(self, other: T) -> Coords<T> {
    Coords {
      x: self.x * other,
      y: self.y * other,
      z: self.z * other,
    }
  }
}

impl<T: Float > Div<T> for Coords<T> {
  type Output = Self;

  fn div(self, other: T) -> Self {
           // 1 / other
    self * ( other.recip() )
  }
}

impl<T: Sub<Output = T>> Sub for Coords<T> {
  type Output = Self;

  fn sub(self, other: Self) -> Self {
    Coords {
      x: self.x - other.x,
      y: self.y - other.y,
      z: self.z - other.z,
    }
  }
}

impl<T: Neg<Output = T>> Neg for Coords<T> {
  type Output = Coords<T>;

  fn neg(self) -> Self::Output {
    Coords {
      x: -self.x,
      y: -self.y,
      z: -self.z,
    }
  }
}

impl<T: Float> Coords<T> {
  fn length(&self) -> T {
    self.length_sq().sqrt()
  }

  pub fn length_sq(&self) -> T {
    self.x.powi(2) + self.y.powi(2) + self.z.powi(2)
  }

  pub fn unit_vector(&self) -> Self {
    *self / self.length()
  }

  // Dot product (https://en.wikipedia.org/wiki/Dot_product)
  pub fn dot(&self, other: &Coords<T>) -> T {
    self.x * other.x + self.y * other.y + self.z * other.z
  }
}

pub type Point3<T> = Coords<T>;
pub type Vec3<T> = Coords<T>;
