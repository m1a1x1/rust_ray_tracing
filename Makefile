small: render-small.ppm
	eog render-small.ppm

big: render-big.ppm
	eog render-big.ppm

render-small.ppm: src/*
	cargo run -- -F render-small.ppm -W 400 -H 225

render-big.ppm: src/*
	cargo run -- -F render-big.ppm -W 1920 -H 1080

clean:
	rm render-big.ppm
	rm render-small.ppm
	cargo clean
